module DatetimeSplitter
	def split_datetime(field, options={})

		define_method("#{field}_date=") do |date|
			date = Date.parse(date.to_s)
			new_date =  (self.send("#{field}") || DateTime.new).change(year: date.year, month: date.month, day: date.day)
			self.send("#{field}=",new_date)
		end
		
		define_method("#{field}_date") do 
			date = self.send(field).try :to_date
      		options[:date_format] ? date.try(:strftime,options[:date_format]) : date
		end

		define_method("#{field}_time=") do |time|
			time = DateTime.parse(time.to_s)
			new_time =  (self.send("#{field}") || DateTime.new).change(hour: time.hour, min: time.min, sec: time.sec)
			self.send("#{field}=",new_time)
		end

		define_method("#{field}_time") do 
			time = self.send(field)
      		options[:time_format] ? time.try(:strftime,options[:time_format]) : time
		end

	end
end