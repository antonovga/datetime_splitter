require 'datetime_splitter'
require 'minitest/autorun'
require 'active_support/all'
require 'date'
class Model
  extend DatetimeSplitter
  attr_accessor :created_at
  split_datetime :created_at
end

class FormattedModel
  extend DatetimeSplitter
  attr_accessor :created_at
  split_datetime :created_at, date_format: '%d/%m/%Y', time_format: '%H:%M'
end

class DatetimeSplitterTest < Minitest::Test

  def setup
  @model = Model.new
  end
  
  def test_respond_to_date_getter  
    assert_respond_to @model, :created_at_date
  end
  
  def test_respond_to_date_setter
    assert_respond_to @model, :created_at_date=
  end

  def test_respond_to_time_setter
    assert_respond_to @model, :created_at_time=
  end
  
  def test_respond_to_time_getter    
    assert_respond_to @model, :created_at_time
  end

  def test_set_date
    date = DateTime.new.to_date
    @model.created_at_date = date 
    assert_equal @model.created_at, date
  end

  def test_set_time
    time = DateTime.new
    @model.created_at_time = time 
    assert_equal @model.created_at, time
  end

  def test_set_date_from_string
    @model.created_at_date = '12/03/2015'
    assert_equal @model.created_at, DateTime.new(2015,3,12)
  end

  def test_set_time_from_string
    @model.created_at_time = '12:34:56'
    assert_equal @model.created_at, DateTime.new(-4712,1,1,12,34,56)
  end

  def test_date_format
    formatted_model = FormattedModel.new
    formatted_model.created_at_date = '12/02/2015'
    assert_equal formatted_model.created_at_date, '12/02/2015'
  end

  def test_time_format
    formatted_model = FormattedModel.new
    formatted_model.created_at_time = '12:34:56'
    assert_equal formatted_model.created_at_time, '12:34'
  end

  def test_handle_nil_formatted_date
    formatted_model = FormattedModel.new
    assert_equal formatted_model.created_at_date, nil
  end

  def test_handle_nil_non_formatted_date
    model = Model.new
    assert_equal model.created_at_date, nil
  end

  def test_handle_nil_formatted_time
    formatted_model = FormattedModel.new
    assert_equal formatted_model.created_at_time, nil
  end

  def test_handle_nil_non_formatted_time
    model = Model.new
    assert_equal model.created_at_time, nil
  end

end