# DatetimeSplitter

Splits datetime field into date & time fields

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'datetime_splitter'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install datetime_splitter

## Usage

```ruby
class Foo < ActiveRecord::Base
	extend DatetimeSplitter
	split_datetime :created_at, date_format: '%d/%m/%Y', time_format: '%H:%M:%S'
end
```
You'll get accessor methods to use in forms

`Foo.new.created_at_date`
`Foo.new.created_at_date=`
`Foo.new.created_at_time`
`Foo.new.created_at_time=`

## Contributing

1. Fork it ( https://bitbucket.org/antonovga/datetime_splitter/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
