# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'datetime_splitter/version'

Gem::Specification.new do |spec|
  spec.name          = "datetime_splitter"
  spec.version       = DatetimeSplitter::VERSION
  spec.authors       = ["Antonov Grigory"]
  spec.email         = ["antonovga@gmail.com"]
  spec.summary       = %q{Splits datetime field into date & time}
  spec.description   = %q{Splits datetime field into date & time fields}
  spec.homepage      = "https://bitbucket.org/antonovga/datetime_splitter"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "minitest", '~>5.0'
  spec.add_development_dependency "rake", "~> 10.0"
end
